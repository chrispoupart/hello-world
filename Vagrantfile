# This Vagrantfile sets up a RHEL 8 host that can be used to run Ansible
# playbooks.
#
# To use, copy this to the base of your Ansible project, make sure it is named
# "Vagrantfile", and then from the commandline, run "vagrant up"
#
# Note that as with most Vagrant boxes, the directory that contains the
# Vagrantfile will be synced to the VM under /vagrant. Once you login to the box
# (by running "vagrant ssh"), you must activate your Python Virtual Environment
# by running "source ~/ansible/bin/activate". You can then change directories to
# /vagrant (cd /vagrant). Install your roles and collections (ansible-galaxy
# install -r requirements.yml) and run your playbook.
#
# To run the playbook against the virtual machine (a good test environment),
# run: ansible-playbook -i inventory/test.ini site.yml

Vagrant.configure("2") do |config|

  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  config.vm.box = "mcgill/rhel8.next"

  # The hostname of the server. Change this to match your project.
  config.vm.hostname = "ansible-host.localdomain"

  # Validate the proper registration plugin is installed, then register
  config.vagrant.plugins = ["vagrant-registration"]

  config.registration.org = 'ITS'
  config.registration.name = 'ansible-host.localdomain'
  config.registration.activationkey = 'vagrant-el8'

  # Forward your SSH Agent to the Vagrant Box!
  config.ssh.forward_agent = true

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for Virr "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  config.vm.provision "shell", inline: <<-SHELL
    # Set up a line in the vagrant user's .bashrc to give instructions on login.
    echo 'echo -e \"\nTo activate your Ansible environment, run\n\tsource ~/ansible/bin/activate\"\n' >> /home/vagrant/.bashrc

    # This installs python38, an upcoming requirement for Ansible
    yum install -y python38 python38-pip

    # Create a python virtual environment. This is the recommended way to do Python
    # see: https://realpython.com/python-virtual-environments-a-primer/
    python3 -m venv ansible

    # Activate the virtual environment
    source ansible/bin/activate

    # Upgrade the pip pacakge to the newest version, a requirement to
    # install crypto libraries in Python that rely on Rust.
    pip3 install --upgrade pip

    # Install Python Wheels. This just makes python packages installation "work"
    # see: https://realpython.com/python-wheels/
    pip install wheel
    pip install ansible ansible-lint

    # Tell all Python instances to use the system CA Certificates instead of
    # the Python requests package CA certificates. This is required to use the
    # PKI Campus certificate bundled with mcgill/rhel8.next >= 8.4.1.
    echo 'export REQUESTS_CA_BUNDLE="/etc/pki/tls/certs/ca-bundle.crt"' \
      | sudo tee /etc/profile.d/ca_certs.sh

    # Some Ansible modules require additional python packages to be installed.
    # The standard is to document these requirements in a file called 'requirements.txt'
    # This tests for the presense of that file, and if it exists, will install the
    # required packages in the virtual environment right away.
    [[ -f /vagrant/requirements.txt ]] && pip install -r /vagrant/requirements.txt || exit 0
  SHELL
end
# -*- mode: ruby -*-
# vi: set ft=ruby :
