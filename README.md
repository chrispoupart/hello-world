# Ansible Playbook

## Introduction

This is a boilerplate configuration. It's strongly recommended that you update
this file to provide proper documentation for your project.

In general, you will want to provide a description of your project, as well as
instructions on how to run this project and how to make contributions.

## Development Workflow

This project is setup by default to use a local Vagrant box for development.

If you don't have Vagrant installed and configured, you should start with our
Blog series [Developing Ansible
Playbooks](https://ncs.pages.it.mcgill.ca/handbook/blog/2021-12-06-developing-ansible-part-one/).

### Git Pre Commit Hooks

It's best practice to test your code before committing it. To help facilitate
this, we recommend the use of [pre-commit](https://pre-commit.com/).

To use it, you should install pre-commit on your development environment:

```shell
pip3 install --user pre-commit
```

Once this is done once on your computer, you need only install the pre-commit
hooks as defined in [.pre-commit-config.yaml](.pre-commit-config.yaml) by
running in the root of your project:

```shell
pre-commit install
```

After that, the defined tests will run automatically before every git commit,
fixing or flagging a gamut of common syntax and linting problems before you
commit the code.

## Resources

* [MS Teams Ansible knowledge share
  team](https://teams.microsoft.com/l/team/19%3ad1bfbe4ba6ab4b2b94725df43e7cbe5d%40thread.tacv2/conversations?groupId=75f63327-2fd1-4d99-828d-8c62e9bddc4e&tenantId=cd319671-52e7-4a68-afa9-fcf8f89f09ea)
* [Shared McGill Ansible Resources](https://gitlab.ncs.mcgill.ca/ansible)
* [ITS Blog Posts about Ansible](https://ncs.pages.it.mcgill.ca/handbook/tags/#ansible)
* [Ansible Best Practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)
* [GitLab CI Template for Ansible](https://gitlab.ncs.mcgill.ca/gitlab/templates#ansible-playbooks)
